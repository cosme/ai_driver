var t = 0;
var cars = [];
var obstacles = [];
var bck = {};
bck.numlines = 3;
var maxSpeed = 10;
var frontView = 4;
var backView = 3;
var s = {};

// create environment
var env = {};
env.getNumStates = function() { return 8; }
env.getMaxNumActions = function() { return 5; }

// agent parameter spec to play with (this gets eval()'d on Agent reset)
var spec = {}
spec.update = 'qlearn'; // qlearn | sarsa
spec.gamma = 0.9; // discount factor, [0, 1)
spec.epsilon = 0.2; // initial epsilon for epsilon-greedy policy, [0, 1)
spec.alpha = 0.05; // value function learning rate
spec.experience_add_every = 20; // number of time steps before we add another experience to replay memory
spec.experience_size = 10000; // size of experience
spec.learning_steps_per_iteration = 20;
spec.tderror_clamp = 1.0; // for robustness
spec.num_hidden_units = 100 // number of neurons in hidden layer

agent = new RL.DQNAgent(env, spec); 


// deepqlearn.js
var brain = new deepqlearn.Brain(8, 5); // 8 inputs, 2 possible outputs (0,1)

brain.epsilon_test_time = 0.01;

function setup() {
	collideDebug(true);
	bck.roadLeft = floor(windowHeight / 3);
	bck.roadRight = floor(2 * windowHeight / 3);
	//frameRate(30);

	cars.push({
		x: 10,
		line: 1,
		speed: 1,
		color: 'blue'
	});
	/*cars.push({
		x: 10,
		line: 2,
		speed: 1,
		color: 'red'
	});
	/*cars.push({
		x: 10,
		line: 3,
		speed: 1,
		color: 'green'
	});
	cars.push({
		x: 10,
		line: 4,
		speed: 1,
		color: 'orange'
	});
	cars.push({
		x: 10,
		line: 5,
		speed: 1,
		color: 'purple'
	});
	cars.push({
		x: 10,
		line: 6,
		speed: 1,
		color: 'black'
	});
	//*/
	obstacles.push({
		x: 2*windowWidth / 4,
		line: 1
	});
	obstacles.push({
		x: 3 * windowWidth / 4,
		line: 2
	});
	/*obstacles.push({
		x: 3 * windowWidth / 4,
		line: 3
	});
	obstacles.push({
		x: 2 * windowWidth / 4,
		line: 4
	});
	obstacles.push({
		x: 1 * windowWidth / 4,
		line: 5
	});
	//*/
	createCanvas(windowWidth, windowHeight);
	background(100);
	bck.lineWidth = floor((bck.roadRight - bck.roadLeft) / bck.numlines);
	s.ARC_RADIUS_FRONT = frontView * bck.lineWidth / 2;
	s.ARC_ANGLE_FRONT = PI / 10;
	s.ROTATION_ANGLE_FRONT = 7*PI/12;
	s.ARC_RADIUS_BACK = backView * bck.lineWidth / 2;
	s.ARC_ANGLE_BACK = -7 * PI / 8;
	s.ROTATION_ANGLE_BACK = -HALF_PI;
	cars.forEach(function (car, i, cars) {
		cars[i].targetLine = car.line;
		cars[i].reward = 0;
	});
	cars.forEach(function (car, i, cars) {
		cars[i].y = bck.roadLeft + (car.line - 0.5) * bck.lineWidth
	});
	obstacles.forEach(function (obstacle, i, obstacles) {
		obstacles[i].y = bck.roadLeft + (obstacle.line - 0.5) * bck.lineWidth
	});
}

function drawcars() {
	cars.forEach(function (car) {
		noFill();
		stroke(car.color);
		if (car.sensors.front_right) {
			arc(car.x, car.y, 2 * s.ARC_RADIUS_FRONT, 2 * s.ARC_RADIUS_FRONT, s.ARC_ANGLE_FRONT, s.ROTATION_ANGLE_FRONT, PIE);
		}
		if (car.sensors.front_left) {
			arc(car.x, car.y, 2 * s.ARC_RADIUS_FRONT, 2 * s.ARC_RADIUS_FRONT, -s.ROTATION_ANGLE_FRONT, -s.ARC_ANGLE_FRONT, PIE);
		}
		if (car.sensors.frontDist < 100) {
			rect(car.x, car.y - bck.lineWidth / 2, s.ARC_RADIUS_FRONT, bck.lineWidth);
		}
		if (car.sensors.back_right) {
			arc(car.x, car.y, 2 * s.ARC_RADIUS_BACK, 2 * s.ARC_RADIUS_BACK, s.ARC_ANGLE_BACK, s.ROTATION_ANGLE_BACK, PIE);
		}
		if (car.sensors.back_left) {
			arc(car.x, car.y, 2 * s.ARC_RADIUS_BACK, 2 * s.ARC_RADIUS_BACK, -s.ROTATION_ANGLE_BACK, -s.ARC_ANGLE_BACK, PIE);
		}
		//rect(car.x, car.y-bck.lineWidth/2,-s.ARC_RADIUS_BACK,bck.lineWidth);
		fill(car.color);
		ellipse(car.x, car.y, bck.lineWidth / 2);
		fill('white');
		text(car.speed, car.x, car.y);
		stroke('white');
	});
	// print(cars[0].x);
}

function readSensors(car) {
	var sensors = {};
	toDetect = obstacles.concat(cars);
	sensors.front_left = toDetect.some(function (el) {
			return collidePointArc(el.x - 1, el.y, car.x, car.y, s.ARC_RADIUS_FRONT, (-s.ARC_ANGLE_FRONT - s.ROTATION_ANGLE_FRONT) / 2, -s.ARC_ANGLE_FRONT + s.ROTATION_ANGLE_FRONT);
		});
	sensors.front_right = toDetect.some(function (el) {
			return collidePointArc(el.x - 1, el.y, car.x, car.y, s.ARC_RADIUS_FRONT, (s.ARC_ANGLE_FRONT + s.ROTATION_ANGLE_FRONT) / 2, -s.ARC_ANGLE_FRONT + s.ROTATION_ANGLE_FRONT);
		});
	sensors.frontDist = 100;
	sensors.frontSpeed = 0;
	toDetect.some(function (el) {
		if (collideRectCircle(car.x, car.y - bck.lineWidth / 2, s.ARC_RADIUS_FRONT, bck.lineWidth, el.x - bck.lineWidth / 4 - 1, el.y, bck.lineWidth / 2)) {
			thisFrontDist = floor((el.x - bck.lineWidth / 4 - 1 - car.x) / (s.ARC_RADIUS_FRONT) * 100);
			if (thisFrontDist < sensors.frontDist) {
				sensors.frontDist = thisFrontDist;
				elSpeed = el.speed || 0;
				sensors.frontSpeed = car.speed-elSpeed;
				fill(car.color);
				text(sensors.frontDist, el.x - bck.lineWidth / 2, car.y)
			}
		}
	});
	sensors.back_left = toDetect.some(function (el) {
			return collidePointArc(el.x - 1, el.y, car.x, car.y, s.ARC_RADIUS_BACK, (-s.ARC_ANGLE_BACK - s.ROTATION_ANGLE_BACK) / 2, -s.ARC_ANGLE_BACK + s.ROTATION_ANGLE_BACK);
		});
	sensors.back_right = toDetect.some(function (el) {
			return collidePointArc(el.x - 1, el.y, car.x, car.y, s.ARC_RADIUS_BACK, (s.ARC_ANGLE_BACK + s.ROTATION_ANGLE_BACK) / 2, -s.ARC_ANGLE_BACK + s.ROTATION_ANGLE_BACK);
		});
	return sensors;
}

function carlifes() {
	cars.forEach(function (car, i, cars) {
		initX=car.x;
		reward=0;

		// Read sensors
		car.sensors = readSensors(car);
		//print(car.sensors.front);
		car.sensorsArray=[
			car.sensors.front_left ? 1 : 0 ,
			car.sensors.front_right ? 1 : 0 ,
			car.sensors.frontDist,
			car.sensors.frontSpeed,
			car.sensors.back_left ? 1 : 0 ,
			car.sensors.back_right ? 1 : 0 ,
			(car.line==1) ? 0 : (car.line==bck.numlines ? 1 : 0.5),
			car.speed
		]

		//var action = agent.act(car.sensorsArray); 
		//if(t%10 == 1) 
			var action = brain.forward(car.sensorsArray); 
		
		// Decisions : should depend on the sensors
		acceleration=0;
		lineSwitch=0;
		//acceleration = floor(random(-0.1, 1.2));
		//lineSwitch = floor(random(-0.005, 1.005));
		if(action == 1) acceleration = 1;
		if(action == 2) acceleration = -1;
		if(action == 3) {
			lineSwitch = 1;
			reward -= 100;
		}
		if(action == 4) {
			lineSwitch = -1;
			reward -= 100;
		}

		// Manage speed
		car.speed = car.speed + acceleration;
		if (car.speed < 1) {
			car.speed = 1
		}
		if (car.speed > maxSpeed) {
			car.speed = maxSpeed
		}
		car.x = (car.x + car.speed/5);

		// Manage line
		car.line = car.line+lineSwitch;
		if (car.line < 1) {
				car.line = 1
			}
			if (car.line > bck.numlines) {
				car.line = bck.numlines
			}
		//car.targetLine = car.line+lineSwitch;
		/*if (car.line == car.targetLine) {
			car.targetLine = car.targetLine + lineSwitch;
			if (car.targetLine < 1) {
				car.targetLine = 1
			}
			if (car.targetLine > bck.numlines) {
				car.targetLine = bck.numlines
			}
		} else if (car.targetLine > car.line) {
			car.line = car.line + 0.1 * car.speed / maxSpeed;
			if (car.line > car.targetLine) {
				car.line = car.targetLine
			}
		} else {
			car.line = car.line - 0.1 * car.speed / maxSpeed;
			if (car.line < car.targetLine) {
				car.line = car.targetLine
			}
		}
		*/
		car.y = bck.roadLeft + (car.line - 0.5) * bck.lineWidth;

		// Collision with other car
		cars.forEach(function (other, o) {
			if (o !== i && collideCircleCircle(car.x, bck.roadLeft + (car.line - 0.5) * bck.lineWidth, bck.lineWidth / 2, other.x, bck.roadLeft + (other.line - 0.5) * bck.lineWidth, bck.lineWidth / 2, bck.lineWidth / 2)) {
				car.speed = 0;
				car.x = (car.x - bck.lineWidth / 2);
				reward -= 300 ;
			}
		});

		// Collision with obstacle
		obstacles.forEach(function (obs) {
			if (collideCircleCircle(car.x, bck.roadLeft + (car.line - 0.5) * bck.lineWidth, bck.lineWidth / 2, obs.x, bck.roadLeft + (obs.line - 0.5) * bck.lineWidth, bck.lineWidth / 2, bck.lineWidth / 2)) {
				car.speed = 0;
				car.x = (car.x - bck.lineWidth);
				reward -= 300 ;
			}
		});

		reward += (car.x-initX)*5;
		
		// Handle negative positions
		if (car.x < 0) {
			car.x = 0;
			car.targetLine = floor(random(1, bck.numlines + 1));
			car.line = car.targetLine;
		} else if (car.x>windowWidth) {
			car.x = car.x % windowWidth;
		}
		
		car.reward += reward;
		
		//agent.learn(reward/100);
		//if(t%10 == 1) brain.backward([reward/100]);
		//if(t%10 == 1) 
			brain.backward([reward/100]);
		
		// Save changes
		cars[i] = car;
	})
}

function bckgnd() {
	background(100);
	// Draw road sides
	stroke('white');
	strokeWeight(10);
	line(0, bck.roadLeft, windowWidth, bck.roadLeft);
	line(0, bck.roadRight, windowWidth, bck.roadRight);
	// Draw lane separations
	for (i = 1; i < bck.numlines; i++) {
		strokeWeight(1);
		stroke(255);
		line(0, bck.roadLeft + i * bck.lineWidth, windowWidth, bck.roadLeft + i * bck.lineWidth);
	}
	// Draw obstacles
	obstacles.forEach(function (obs) {
		fill('white');
		ellipse(obs.x, bck.roadLeft + (obs.line - 0.5) * bck.lineWidth, bck.lineWidth / 2);
	});
	// Draw stats
	text('Time : '+t,10,10);
	cars.forEach(function (car, i) {
		stroke(car.color);
		text(JSON.stringify(car.sensors),50, 20*(i+2));
		text(JSON.stringify(car.reward),750, 20*(i+2));
	});

}

function draw() {
	t = t + 1;
	carlifes();
	bckgnd();
	drawcars();
}
